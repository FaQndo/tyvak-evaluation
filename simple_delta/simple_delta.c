#include "simple_delta.h"
#include "simple_delta_internals.h"
#include <ctype.h>

#define LETTERS_COUNT   26

void simple_delta(FILE *f1, FILE *f2, FILE *out)
{
    if (feof(f1) && feof(f2)) {
        return;
    }

    do {
        char out_char = simple_delta_get_next_output_character(f1, f2);
        if (out_char != '\0') {
            fputc(out_char, out);
        }
    } while (!feof(f1) || !feof(f2));
}

char simple_delta_get_next_output_character(FILE *f1, FILE *f2)
{
    char c1 = simple_delta_get_next_character(f1);
    char c2 = simple_delta_get_next_character(f2);

    if (c1 == '\0') {
        return c2;
    } else if (c2 == '\0') {
        return c1;
    } else {
        return simple_delta_char_delta(c1, c2);
    }
}

char simple_delta_char_delta(char c1, char c2)
{
    char delta = c1 - c2;
    if (delta == 0) {
        return '\0';
    }
    if (delta < 0) {
        delta += LETTERS_COUNT + 1;
    }
    return 'a' + delta - 1;
}

char simple_delta_get_next_character(FILE *f)
{
    while (!feof(f)) {
        char c = fgetc(f);
        if (isalpha(c)) {
            return c;
        }
    }

    return '\0';
}

