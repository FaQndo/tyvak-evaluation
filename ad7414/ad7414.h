#ifndef _AD7414_H_
#define _AD7414_H_

#include <stdint.h>
#include <stdbool.h>

typedef enum {
    ACTIVE_HIGH,
    ACTIVE_LOW,
} ad7414_polarity_e;

typedef enum {
    REGISTER_TEMPERATURE,
    REGISTER_CONFIG,
    REGISTER_T_HIGH,
    REGISTER_T_LOW
} ad7414_register_e;

#define AD7414_ADDRESS  0x48

int ad7414_set_t_high_C(uint8_t value);
int ad7414_set_t_low_C(uint8_t value);
int ad7414_set_alert_enable(bool value);
int ad7414_set_alert_polarity(ad7414_polarity_e);
int ad7414_set_power_down(bool value);
int ad7414_reset_alert(void);
int ad7414_get_temperature_C(float *value);
int ad7414_get_one_shot_temperature_C(float *value);

int ad7414_set_config_bits(uint8_t bits, uint8_t new_values);
int ad7414_set_config_bits_full_transaction(uint8_t bits, uint8_t new_values);
int ad7414_write_register(ad7414_register_e reg, uint8_t value);

#endif
