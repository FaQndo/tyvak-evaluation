#ifndef _SIMPLE_DELTA_H_
#define _SIMPLE_DELTA_H_

#include <stdio.h>

void simple_delta(FILE *f1, FILE *f2, FILE *out);

#endif
