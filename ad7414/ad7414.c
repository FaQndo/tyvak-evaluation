#include "ad7414.h"
#include "i2c.h"

typedef union {
    uint8_t full;
    struct {
#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
        int POWER_DOWN     : 1;
        int FILTER         : 1;
        int ALERT_ENABLE   : 1;
        int ALERT_POLARITY : 1;
        int ALERT_RESET    : 1;
        int ONE_SHOT       : 1;
        int TEST_MODE      : 2;
#else
        int TEST_MODE      : 2;
        int ONE_SHOT       : 1;
        int ALERT_RESET    : 1;
        int ALERT_POLARITY : 1;
        int ALERT_ENABLE   : 1;
        int FILTER         : 1;
        int POWER_DOWN     : 1;
#endif
    };
} ad7414_config_register_t;

#define RETURN_IF_ERROR(rv)   do { if (rv < 0) { return rv; } } while(false);

int ad7414_write_register(ad7414_register_e reg, uint8_t value)
{
    int rv = i2c_open();
    RETURN_IF_ERROR(rv);
    char command[] = {reg, value};
    rv = i2c_write(command, sizeof(command), AD7414_ADDRESS);
    RETURN_IF_ERROR(rv);
    return i2c_close();
}

int ad7414_set_t_high_C(uint8_t value)
{
    return ad7414_write_register(REGISTER_T_HIGH, value);
}

int ad7414_set_t_low_C(uint8_t value)
{
    return ad7414_write_register(REGISTER_T_LOW, value);
}

int ad7414_set_power_down(bool value)
{
    ad7414_config_register_t bit = { .full = 0 };
    ad7414_config_register_t new_value = { .full = 0 };

    bit.POWER_DOWN = 1;
    new_value.POWER_DOWN = value;

    return ad7414_set_config_bits_full_transaction(bit.full, new_value.full);
}

int ad7414_get_temperature_C(float *value)
{
    uint8_t read_register = REGISTER_TEMPERATURE;
    uint8_t msb_temp_value;
    uint8_t lsb_temp_value;
    int16_t temp_value;

    int rv = i2c_open();
    RETURN_IF_ERROR(rv);

    rv = i2c_write(&read_register, 1, AD7414_ADDRESS);
    RETURN_IF_ERROR(rv);
    /* NOTE: So we avoid endianness issues */
    rv = i2c_read(&msb_temp_value, 1, AD7414_ADDRESS);
    RETURN_IF_ERROR(rv);
    rv = i2c_read(&lsb_temp_value, 1, AD7414_ADDRESS);
    RETURN_IF_ERROR(rv);

    temp_value = (msb_temp_value << 2) | (lsb_temp_value >> 6);
    temp_value |= ((temp_value & (1 << 9)) ? 0xFC00 : 0);
    *value = temp_value * .25f;

    return i2c_close();
}

int ad7414_get_one_shot_temperature_C(float *value)
{
    ad7414_config_register_t bit = { .full = 0 };
    ad7414_config_register_t new_value = { .full = 0 };

    bit.ONE_SHOT = 1;
    new_value.ONE_SHOT = 1;

    int rv = i2c_open();
    RETURN_IF_ERROR(rv);;
    rv = ad7414_set_config_bits(bit.full, new_value.full);
    RETURN_IF_ERROR(rv);;
    rv = ad7414_get_temperature_C(value);
    RETURN_IF_ERROR(rv);;
    return i2c_close();
}

int ad7414_set_alert_enable(bool value)
{
    ad7414_config_register_t bit = { .full = 0 };
    ad7414_config_register_t new_value = { .full = 0 };

    bit.ALERT_ENABLE = 1;
    new_value.ALERT_ENABLE = value;

    return ad7414_set_config_bits_full_transaction(bit.full, new_value.full);
}

int ad7414_reset_alert(void)
{
    ad7414_config_register_t bit = { .full = 0 };
    ad7414_config_register_t new_value = { .full = 0 };

    bit.ALERT_RESET = 1;
    new_value.ALERT_RESET = 1;

    return ad7414_set_config_bits_full_transaction(bit.full, new_value.full);
}

int ad7414_set_alert_polarity(ad7414_polarity_e p)
{
    ad7414_config_register_t bit = { .full = 0 };
    ad7414_config_register_t new_value = { .full = 0 };

    bit.ALERT_POLARITY = 1;
    new_value.ALERT_POLARITY = p;

    return ad7414_set_config_bits_full_transaction(bit.full, new_value.full);
}

int ad7414_set_config_bits_full_transaction(uint8_t bits, uint8_t new_values)
{
    int rv = i2c_open();
    RETURN_IF_ERROR(rv);
    rv = ad7414_set_config_bits(bits, new_values);
    RETURN_IF_ERROR(rv);
    return i2c_close();
}

int ad7414_set_config_bits(uint8_t bits, uint8_t new_values)
{
    uint8_t current_config;
    uint8_t config_register = REGISTER_CONFIG;

    int rv = i2c_write(&config_register, 1, AD7414_ADDRESS);
    RETURN_IF_ERROR(rv);
    rv = i2c_read(&current_config, 1, AD7414_ADDRESS);
    RETURN_IF_ERROR(rv);

    current_config = new_values | (current_config & ~bits);
    return i2c_write(&current_config, 1, AD7414_ADDRESS);
}

