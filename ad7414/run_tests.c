#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "unity.h"
#include "ad7414.h"
#include "mock_i2c.h"
//#include "ad7414_internals.h"

void setUp(void)
{
    mock_i2c_Init();
}

void tearDown(void)
{
    mock_i2c_Verify();
}

void test_ad7414_write_register(void)
{
    i2c_open_ExpectAndReturn(0);
    i2c_close_ExpectAndReturn(0);
    uint8_t t_value = 23;
    char command[] = {REGISTER_T_LOW, t_value};
    i2c_write_ExpectWithArrayAndReturn(command,         /* buf */
                                       sizeof(command),
                                       sizeof(command), /* size */
                                       AD7414_ADDRESS,  /* address */
                                       sizeof(command));

    int rv = ad7414_write_register(REGISTER_T_LOW, t_value);

    TEST_ASSERT_EQUAL(0, rv);
}

void test_ad7414_write_register_open_error(void)
{
    i2c_open_ExpectAndReturn(-1);

    int rv = ad7414_write_register(REGISTER_T_LOW, 23);

    TEST_ASSERT_EQUAL(-1, rv);
}

void test_ad7414_write_register_close_error(void)
{
    i2c_open_ExpectAndReturn(0);
    i2c_close_ExpectAndReturn(-1);
    uint8_t t_value = 23;
    char command[] = {REGISTER_T_LOW, t_value};
    i2c_write_ExpectWithArrayAndReturn(command,         /* buf */
                                       sizeof(command),
                                       sizeof(command), /* size */
                                       AD7414_ADDRESS,  /* address */
                                       sizeof(command));

    int rv = ad7414_write_register(REGISTER_T_LOW, t_value);

    TEST_ASSERT_EQUAL(-1, rv);
}

void test_ad7414_write_register_write_error(void)
{
    i2c_open_ExpectAndReturn(0);
    uint8_t t_value = 23;
    char command[] = {REGISTER_T_LOW, t_value};
    i2c_write_ExpectWithArrayAndReturn(command,         /* buf */
                                       sizeof(command),
                                       sizeof(command), /* size */
                                       AD7414_ADDRESS,  /* address */
                                       -1);

    int rv = ad7414_write_register(REGISTER_T_LOW, t_value);

    TEST_ASSERT_EQUAL(-1, rv);
}

void test_ad7414_set_config_bits_write_full_transaction(void)
{
    i2c_open_ExpectAndReturn(0);
    i2c_close_ExpectAndReturn(0);
    uint8_t config_reg = REGISTER_CONFIG;
    i2c_write_ExpectWithArrayAndReturn(&config_reg,     /* buf */
                                       1,
                                       1,               /* size */
                                       AD7414_ADDRESS,  /* address */
                                       1);
    i2c_read_ExpectAndReturn(0,               /* buf, ignored */
                             1,               /* size */
                             AD7414_ADDRESS,  /* address */
                             1);
    i2c_read_IgnoreArg_buf();
    uint8_t old_config = (1 << 6) |
                         (1 << 3) |
                         (1 << 2);
    i2c_read_ReturnThruPtr_buf(&old_config);
    uint8_t new_config = (1 << 6) |
                         (1 << 4) |
                         (1 << 2) |
                         1;
    i2c_write_ExpectWithArrayAndReturn(&new_config,     /* buf */
                                       1,
                                       1,               /* size */
                                       AD7414_ADDRESS,  /* address */
                                       1);
    ad7414_set_config_bits_full_transaction((1 << 4) | (1 << 3) | 1, (1 << 4) | 1);
}

void test_ad7414_set_config_bits_write(void)
{
    uint8_t config_reg = REGISTER_CONFIG;
    i2c_write_ExpectWithArrayAndReturn(&config_reg,     /* buf */
                                       1,
                                       1,               /* size */
                                       AD7414_ADDRESS,  /* address */
                                       1);
    i2c_read_ExpectAndReturn(0,               /* buf, ignored */
                             1,               /* size */
                             AD7414_ADDRESS,  /* address */
                             1);
    i2c_read_IgnoreArg_buf();
    uint8_t old_config = (1 << 6) |
                         (1 << 3) |
                         (1 << 2);
    i2c_read_ReturnThruPtr_buf(&old_config);
    uint8_t new_config = (1 << 6) |
                         (1 << 4) |
                         (1 << 2) |
                         1;
    i2c_write_ExpectWithArrayAndReturn(&new_config,     /* buf */
                                       1,
                                       1,               /* size */
                                       AD7414_ADDRESS,  /* address */
                                       1);
    ad7414_set_config_bits((1 << 4) | (1 << 3) | 1, (1 << 4) | 1);
}

void test_ad7414_get_temperature_C_pos(void)
{
    i2c_open_ExpectAndReturn(0);
    i2c_close_ExpectAndReturn(0);
    uint8_t read_reg = REGISTER_TEMPERATURE;
    i2c_write_ExpectWithArrayAndReturn(&read_reg,       /* buf */
                                       1,
                                       1,               /* size */
                                       AD7414_ADDRESS,  /* address */
                                       1);
    i2c_read_ExpectAndReturn(0,               /* buf, ignored */
                             1,               /* size */
                             AD7414_ADDRESS,  /* address */
                             1);
    i2c_read_IgnoreArg_buf();
    uint8_t msb = 0x32; /* 00110010 */
    i2c_read_ReturnThruPtr_buf(&msb);
    i2c_read_ExpectAndReturn(0,               /* buf, ignored */
                             1,               /* size */
                             AD7414_ADDRESS,  /* address */
                             1);
    i2c_read_IgnoreArg_buf();
    uint8_t lsb = 1 << 6;
    i2c_read_ReturnThruPtr_buf(&lsb);
    float value = 0.f;

    ad7414_get_temperature_C(&value);
    TEST_ASSERT_EQUAL_FLOAT(50.25, value);
}

void test_ad7414_get_temperature_C_neg(void)
{
    i2c_open_ExpectAndReturn(0);
    i2c_close_ExpectAndReturn(0);
    uint8_t read_reg = REGISTER_TEMPERATURE;
    i2c_write_ExpectWithArrayAndReturn(&read_reg,       /* buf */
                                       1,
                                       1,               /* size */
                                       AD7414_ADDRESS,  /* address */
                                       1);
    i2c_read_ExpectAndReturn(0,               /* buf, ignored */
                             1,               /* size */
                             AD7414_ADDRESS,  /* address */
                             1);
    i2c_read_IgnoreArg_buf();
    uint8_t msb = 0xcd; /* 11001101 */
    i2c_read_ReturnThruPtr_buf(&msb);
    i2c_read_ExpectAndReturn(0,               /* buf, ignored */
                             1,               /* size */
                             AD7414_ADDRESS,  /* address */
                             1);
    i2c_read_IgnoreArg_buf();
    uint8_t lsb = 2 << 6;
    i2c_read_ReturnThruPtr_buf(&lsb);
    float value = 0.f;

    ad7414_get_temperature_C(&value);
    TEST_ASSERT_EQUAL_FLOAT(-50.5f, value);
}

int main(void)
{
    UNITY_BEGIN();
    RUN_TEST(test_ad7414_write_register);
    RUN_TEST(test_ad7414_write_register_open_error);
    RUN_TEST(test_ad7414_write_register_close_error);
    RUN_TEST(test_ad7414_write_register_write_error);

    RUN_TEST(test_ad7414_set_config_bits_write);
    RUN_TEST(test_ad7414_set_config_bits_write_full_transaction);

    RUN_TEST(test_ad7414_get_temperature_C_pos);
    RUN_TEST(test_ad7414_get_temperature_C_neg);
    return UNITY_END();
}

