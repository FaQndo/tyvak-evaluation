#ifndef _I2C_H_
#define _I2C_H_

int i2c_open(void);
int i2c_close(void);
int i2c_read(char *buf, int size, char address);
int i2c_write(char *buf, int size, char address);

#endif
