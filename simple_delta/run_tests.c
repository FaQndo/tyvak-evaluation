/* NOTE: For fmemopen() inclusion */
#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include "unity.h"
#include "simple_delta.h"
#include "simple_delta_internals.h"

void test_simple_delta_get_next_character(void) {
    char data[] = "aBc d   ef\ng \nh  ";
    FILE *f = fmemopen(data, sizeof(data), "r");
    TEST_ASSERT_EQUAL('a', simple_delta_get_next_character(f));
    TEST_ASSERT_EQUAL('B', simple_delta_get_next_character(f));
    TEST_ASSERT_EQUAL('c', simple_delta_get_next_character(f));
    TEST_ASSERT_EQUAL('d', simple_delta_get_next_character(f));
    TEST_ASSERT_EQUAL('e', simple_delta_get_next_character(f));
    TEST_ASSERT_EQUAL('f', simple_delta_get_next_character(f));
    TEST_ASSERT_EQUAL('g', simple_delta_get_next_character(f));
    TEST_ASSERT_EQUAL('h', simple_delta_get_next_character(f));
    TEST_ASSERT_EQUAL('\0', simple_delta_get_next_character(f));
    TEST_ASSERT_EQUAL('\0', simple_delta_get_next_character(f));
}

void test_simple_delta_char_delta(void) {
    TEST_ASSERT_EQUAL('o', simple_delta_char_delta('p', 'a'));
    TEST_ASSERT_EQUAL('m', simple_delta_char_delta('o', 'b'));
    TEST_ASSERT_EQUAL('i', simple_delta_char_delta('l', 'c'));
    TEST_ASSERT_EQUAL('x', simple_delta_char_delta('a', 'd'));
    TEST_ASSERT_EQUAL('y', simple_delta_char_delta('z', 'a'));
    TEST_ASSERT_EQUAL('b', simple_delta_char_delta('a', 'z'));
    TEST_ASSERT_EQUAL('\0', simple_delta_char_delta('e', 'e'));
}

void test_simple_delta_get_next_output_character(void) {
    char data1[] = "a  b  c";
    char data2[] = "ab  ahola";

    FILE *f1 = fmemopen(data1, sizeof(data1), "r");
    FILE *f2 = fmemopen(data2, sizeof(data2), "r");

    TEST_ASSERT_EQUAL('\0', simple_delta_get_next_output_character(f1, f2));
    TEST_ASSERT_EQUAL('\0', simple_delta_get_next_output_character(f1, f2));
    TEST_ASSERT_EQUAL('b', simple_delta_get_next_output_character(f1, f2));
    TEST_ASSERT_EQUAL('h', simple_delta_get_next_output_character(f1, f2));
    TEST_ASSERT_EQUAL('o', simple_delta_get_next_output_character(f1, f2));
    TEST_ASSERT_EQUAL('l', simple_delta_get_next_output_character(f1, f2));
    TEST_ASSERT_EQUAL('a', simple_delta_get_next_output_character(f1, f2));
    fclose(f1);
    fclose(f2);

    /* NOTE: The buffers are inverted to test either file can be larger than the other */
    f1 = fmemopen(data2, sizeof(data2), "r");
    f2 = fmemopen(data1, sizeof(data1), "r");

    TEST_ASSERT_EQUAL('\0', simple_delta_get_next_output_character(f1, f2));
    TEST_ASSERT_EQUAL('\0', simple_delta_get_next_output_character(f1, f2));
    TEST_ASSERT_EQUAL('y', simple_delta_get_next_output_character(f1, f2));
    TEST_ASSERT_EQUAL('h', simple_delta_get_next_output_character(f1, f2));
    TEST_ASSERT_EQUAL('o', simple_delta_get_next_output_character(f1, f2));
    TEST_ASSERT_EQUAL('l', simple_delta_get_next_output_character(f1, f2));
    TEST_ASSERT_EQUAL('a', simple_delta_get_next_output_character(f1, f2));

    fclose(f1);
    fclose(f2);
}

void test_simple_delta(void) {
    char data1[] = "polaod";
    char data2[] = "abcd";
    char buffer[sizeof(data1)] = {'\0'};

    FILE *f1 = fmemopen(data1, sizeof(data1), "r");
    FILE *f2 = fmemopen(data2, sizeof(data2), "r");
    FILE *out = fmemopen(buffer, sizeof(buffer), "w");

    simple_delta(f1, f2, out);
    fclose(f1);
    fclose(f2);
    fclose(out);

    TEST_ASSERT_EQUAL_STRING("omixod", buffer);
}

int main(void) {
    UNITY_BEGIN();
    RUN_TEST(test_simple_delta_get_next_character);
    RUN_TEST(test_simple_delta_char_delta);
    RUN_TEST(test_simple_delta_get_next_output_character);
    RUN_TEST(test_simple_delta);
    return UNITY_END();
}

