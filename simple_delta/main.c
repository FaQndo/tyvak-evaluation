#include <stdlib.h>
#include <stdio.h>
#include "simple_delta.h"

#define handle_error(msg) \
        do { perror(msg); exit(EXIT_FAILURE); } while (0)

void usage(const char *program_name)
{
    printf("Usage: %s file1 file2\n", program_name);
}

int main(int argc, char *argv[])
{
    if (argc != 3) {
        usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    FILE *f1 = fopen(argv[1], "r");
    if (f1 == NULL) {
        handle_error("fopen");
    }
    FILE *f2 = fopen(argv[2], "r");
    if (f2 == NULL) {
        handle_error("fopen");
    }

    simple_delta(f1, f2, stdout);
    puts("");

    return EXIT_SUCCESS;
}

