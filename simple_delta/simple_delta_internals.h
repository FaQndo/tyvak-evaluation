#ifndef _SIMPLE_DELTA_INTERNALS_H_
#define _SIMPLE_DELTA_INTERNALS_H_

#include <stdio.h>

char simple_delta_char_delta(char c1, char c2);
char simple_delta_get_next_character(FILE *f);
char simple_delta_get_next_output_character(FILE *f1, FILE *f2);

#endif
